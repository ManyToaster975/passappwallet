package com.manytoaster.emanuelgarcia.passappwallet;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Bloqueo extends AppCompatActivity {

    EditText txtPass;
    Button btnUnlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE);

        setContentView(R.layout.activity_bloqueo);

        btnUnlock=findViewById(R.id.btnDesbloqueo);

        txtPass=findViewById(R.id.txtContrasena);

        btnUnlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SqliteDb sdb=new SqliteDb(getApplicationContext());
                SQLiteDatabase desbloquear=sdb.getReadableDatabase();
                Cursor leer=desbloquear.rawQuery("SELECT Contrasena FROM LISTAUSUARIOS WHERE Contrasena='"+txtPass.getText().toString()+"'", null);
                if(leer.moveToFirst()){
                    Intent acceso=new Intent(Bloqueo.this, Inicio.class);
                    startActivity(acceso);
                    finish();
                }else{
                    Toast.makeText(getApplicationContext(),"Incorrect password", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
