package com.manytoaster.emanuelgarcia.passappwallet;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class Inicio extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String AES="AES";

    private String textoEncriptado;
    private String textoDesencriptado;

    TextView aviso;
    ListView listacontras;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inicio);

        aviso=findViewById(R.id.txtAviso);
        listacontras=findViewById(R.id.listaContrasenas);
        final ArrayList <byte[]> contras=new ArrayList<>();
        SqliteDb leer=new SqliteDb(this);
        SQLiteDatabase llenar=leer.getWritableDatabase();
        Cursor fila=llenar.rawQuery("SELECT * FROM LISTACONTRASENAS",null);
        if(fila.moveToFirst()){
            do{
                contras.add(fila.getBlob(1));
                //Toast.makeText(getApplicationContext(),"id: "+fila.getString(0)+"Codigo: "+ Arrays.toString(fila.getBlob(1)) +"Numero: "+fila.getInt(2),Toast.LENGTH_SHORT).show();
            }while(fila.moveToNext());
        }
        llenar.close();

        ArrayAdapter<byte[]> adaptador=new ArrayAdapter<>(this,android.R.layout.simple_list_item_1,contras);

        if(adaptador.isEmpty()){
            listacontras.setVisibility(View.GONE);
            aviso.setVisibility(View.VISIBLE);
        }else{
            aviso.setVisibility(View.GONE);
            listacontras.setVisibility(View.VISIBLE);
            listacontras.setAdapter(adaptador);
        }

        listacontras.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {

                //String codigo=listacontras.getItemAtPosition(position).toString();
                //Toast.makeText(getApplicationContext(),""+codigo, Toast.LENGTH_LONG).show();

                SqliteDb sdb= new SqliteDb(getApplicationContext());
                SQLiteDatabase leer=sdb.getReadableDatabase();
                Cursor lecturadatos=leer.rawQuery("SELECT Contrasena FROM LISTAUSUARIOS",null);
                if(lecturadatos.moveToNext()){
                    String contraseña=lecturadatos.getString(0);
                    try {

                        Cursor detectar=leer.rawQuery("SELECT Numero, Contrasenas_Lista FROM LISTACONTRASENAS WHERE Numero="+position, null);
                        if(detectar.moveToNext()){
                            String contra_original=detectar.getString(1);
                            int idPass=detectar.getInt(0);
                            //Toast.makeText(getApplicationContext(),"codigo: "+idPass, Toast.LENGTH_LONG).show();
                            textoDesencriptado=desencriptar(contra_original,contraseña);
                        }
                        sdb.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                try{
                    AlertDialog.Builder ver=new AlertDialog.Builder(Inicio.this);

                    final TextView contrasena=new TextView(Inicio.this);
                    contrasena.setText(""+textoDesencriptado);

                    LinearLayout acomodar=new LinearLayout(Inicio.this);
                    acomodar.setOrientation(LinearLayout.VERTICAL);
                    acomodar.addView(contrasena);
                    ver.setView(acomodar);

                    ver.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    ver.show();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/

                final AlertDialog registro = new AlertDialog.Builder(Inicio.this)
                        .setView(view)
                        .setPositiveButton("Guardar", null) //Set to null. We override the onclick
                        .setNegativeButton("Cancelar", null)
                        .setNeutralButton("Vaciar campos", null)
                        .create();

                final EditText pass=new EditText(Inicio.this);
                pass.setGravity(Gravity.CENTER_HORIZONTAL);
                pass.setInputType(InputType.TYPE_CLASS_TEXT);
                pass.setHint("Password");

                LinearLayout acomodar=new LinearLayout(Inicio.this);
                acomodar.setOrientation(LinearLayout.VERTICAL);
                acomodar.addView(pass);
                registro.setView(acomodar);

                registro.setCancelable(false);

                registro.setOnShowListener(new DialogInterface.OnShowListener() {

                    @Override
                    public void onShow(DialogInterface dialogInterface) {

                        Button positivo = (registro).getButton(AlertDialog.BUTTON_POSITIVE);
                        positivo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                SqliteDb sdb= new SqliteDb(getApplicationContext());
                                SQLiteDatabase leer=sdb.getReadableDatabase();
                                Cursor primeraEntrada=leer.rawQuery("SELECT * FROM LISTACONTRASENAS", null);

                                //When the app used more one times
                                if(primeraEntrada.moveToNext()){
                                    Cursor entradas=leer.rawQuery("SELECT Numero FROM LISTACONTRASENAS ORDER BY Numero DESC", null);
                                    if(entradas.moveToNext()){
                                        int id=entradas.getInt(0);
                                        id++;

                                        Cursor lecturadatos=leer.rawQuery("SELECT Contrasena FROM LISTAUSUARIOS",null);
                                        if(lecturadatos.moveToFirst()){
                                            String contrasena=lecturadatos.getString(0);
                                            try {
                                                textoEncriptado=encriptar(pass.getText().toString(),contrasena);
                                                Intent redireccion=new Intent(Inicio.this, Inicio.class);
                                                startActivity(redireccion);
                                                finish();
                                                sdb.close();
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            SQLiteDatabase insertar=sdb.getWritableDatabase();
                                            ContentValues datos=new ContentValues();
                                            datos.put("Contrasenas_lista",textoEncriptado);
                                            datos.put("Numero", id);
                                            insertar.insert("LISTACONTRASENAS",null, datos);
                                            insertar.close();
                                        }
                                    }
                                }else{

                                    //When the app use first time
                                    Cursor lecturadatos=leer.rawQuery("SELECT Contrasena, ClaveUsuario FROM LISTAUSUARIOS",null);
                                    if(lecturadatos.moveToFirst()){
                                        String contrasena=lecturadatos.getString(0);
                                        int id=lecturadatos.getInt(1);
                                        try {
                                            textoEncriptado=encriptar(pass.getText().toString(),contrasena);
                                            Intent redireccion=new Intent(Inicio.this, Inicio.class);
                                            startActivity(redireccion);
                                            finish();
                                            sdb.close();
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                        SQLiteDatabase insertar=sdb.getWritableDatabase();
                                        ContentValues datos=new ContentValues();
                                        datos.put("Contrasenas_lista",textoEncriptado);
                                        datos.put("Numero", id);
                                        insertar.insert("LISTACONTRASENAS",null, datos);
                                        insertar.close();
                                    }
                                }
                                leer.close();

                                registro.dismiss();

                            }
                        });

                        Button neutral = (registro).getButton(AlertDialog.BUTTON_NEUTRAL);
                        neutral.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View view) {
                                pass.setText("");
                            }
                        });

                        Button negativo = (registro).getButton(AlertDialog.BUTTON_NEGATIVE);
                        negativo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                registro.dismiss();
                            }
                        });
                    }
                });
                registro.show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        //A PARTIR DE AQUI SE TRABAJANA CON EL NAVIGATIONBAR
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View hView=navigationView.getHeaderView(0);

        //LOS OBJETOS QUE SE USAN ESTAN ENLAZADOS CON EL XML DEL NAVIGATIONBAR
        TextView correo=hView.findViewById(R.id.correo);

        SqliteDb sdb=new SqliteDb(getApplicationContext());
        SQLiteDatabase lectura=sdb.getReadableDatabase();
        @SuppressLint("Recycle") Cursor mover=lectura.rawQuery("SELECT Correo_Usuario from LISTAUSUARIOS",null);
        if(mover.moveToFirst()){
            String userid=mover.getString(0);
            correo.setText(userid);
        }
    }

    private String desencriptar(String data, String userpass) throws Exception{
        SecretKeySpec secretKeySpec=generateKey(userpass);
        Cipher cipher=Cipher.getInstance(AES);
        cipher.init(Cipher.DECRYPT_MODE,secretKeySpec);
        byte[] datosDecodificados=Base64.decode(data,Base64.DEFAULT);
        byte[] datosDesencriptados=cipher.doFinal(datosDecodificados);
        String datosDesencriptadosString=new String(datosDesencriptados);
        return datosDesencriptadosString;
    }

    private String encriptar(String data, String userpass) throws Exception{
        SecretKeySpec secretKeySpec=generateKey(userpass);
        Cipher cipher=Cipher.getInstance(AES);
        cipher.init(Cipher.ENCRYPT_MODE,secretKeySpec);
        byte[] datosEncriptadosBytes=cipher.doFinal(data.getBytes());
        String datosEncriptadosString=Base64.encodeToString(datosEncriptadosBytes, Base64.DEFAULT);
        return datosEncriptadosString;
    }

    private SecretKeySpec generateKey(String password) throws Exception{
        MessageDigest sha=MessageDigest.getInstance("SHA-256");
        byte[] key=password.getBytes("UTF-8");
        key=sha.digest(key);
        SecretKeySpec secretKeySpec=new SecretKeySpec(key,AES);
        return secretKeySpec;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.inicio, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_bloqueo) {
            Intent traslado = new Intent(Inicio.this, Bloqueo.class);
            startActivity(traslado);
            finish();
        } else if (id == R.id.nav_accounts) {

        } else if (id == R.id.nav_card) {

        } else if (id == R.id.nav_notes) {

        } else if (id == R.id.nav_combination) {

        } else if (id == R.id.nav_others) {

        } else if (id == R.id.nav_options) {
            AlertDialog.Builder opciones=new AlertDialog.Builder(Inicio.this);
            opciones.setTitle("Options");

            final CharSequence[] option=new CharSequence[1];
            option[0]="Crear Backup";
            opciones.setItems(option, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    try {
                        File sd = Environment.getExternalStorageDirectory();
                        File data = Environment.getDataDirectory();
                        String packageName  = "com.manytoaster.emanuelgarcia.passappwallet";
                        String sourceDBName = "passapp.db";
                        String targetDBName = "passapp";
                        if (sd.canWrite()) {
                            Date now = new Date();
                            String currentDBPath = "data/" + packageName + "/databases/" + sourceDBName;
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
                            String backupDBPath = targetDBName + dateFormat.format(now) + ".db";

                            File currentDB = new File(data, currentDBPath);
                            File backupDB = new File(sd, backupDBPath);

                            Log.i("backup","backupDB=" + backupDB.getAbsolutePath());
                            Log.i("backup","sourceDB=" + currentDB.getAbsolutePath());

                            FileChannel src = new FileInputStream(currentDB).getChannel();
                            FileChannel dst = new FileOutputStream(backupDB).getChannel();
                            dst.transferFrom(src, 0, src.size());
                            src.close();
                            dst.close();

                            Toast.makeText(Inicio.this, "Se ha creado el backup en "+backupDBPath, Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Log.i("Backup", e.toString());
                    }
                }
            });

            opciones.setCancelable(false);

            opciones.setNegativeButton("Cancel", null);
            opciones.show();
        } else if (id == R.id.nav_about) {
            AlertDialog.Builder acerca=new AlertDialog.Builder(Inicio.this);
            acerca.setCancelable(false);
            acerca.setMessage("This app save your passwords with encript AES-256"+"\n"+"Please, tell us how do you better this app?");
            acerca.setPositiveButton("Acept", null);
            acerca.show();
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
