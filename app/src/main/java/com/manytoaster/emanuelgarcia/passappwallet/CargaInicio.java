package com.manytoaster.emanuelgarcia.passappwallet;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class CargaInicio extends AppCompatActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);//QUIT NOTIFICATIONS BAR
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,WindowManager.LayoutParams.FLAG_SECURE); //AVOID SCREENSHOTS

        setContentView(R.layout.activity_carga_inicio);

        //APK VERSION
        TextView txtVer = findViewById(R.id.txtVer);
        String version = "1.0"; //Default
        try {
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = packageInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        txtVer.setText("Ver. " + version);

        //CHANGE TEXT STYLE
        Typeface face = Typeface.createFromAsset(getAssets(), "fonts/Rubik-BoldItalic.ttf");
        txtVer.setTypeface(face);

        //GO DIRECTLY TO NEXT ACTIVITY
        int duracion_splash = 5000;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SqliteDb consulta = new SqliteDb(getApplicationContext());
                SQLiteDatabase bd = consulta.getReadableDatabase();
                @SuppressLint("Recycle") Cursor fila = bd.rawQuery("SELECT*FROM LISTAUSUARIOS", null);
                if (fila.moveToNext()) {

                    Intent traslado = new Intent(CargaInicio.this, Bloqueo.class);
                    startActivity(traslado);
                    finish();

                } else {

                    //USER REGISTER
                    View v = new View(CargaInicio.this);
                    final AlertDialog registro = new AlertDialog.Builder(CargaInicio.this)
                            .setView(v)
                            .setPositiveButton("Registrar", null) //Set to null. We override the onclick
                            .setNegativeButton("Cancelar", null)
                            .setNeutralButton("Vaciar campos", null)
                            .create();

                    final TextView medidacorreo = new TextView(CargaInicio.this);
                    medidacorreo.setGravity(Gravity.CENTER_HORIZONTAL);


                    final EditText email = new EditText(CargaInicio.this);
                    email.setGravity(Gravity.CENTER_HORIZONTAL);
                    email.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
                    email.setHint("Email");
                    email.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            if(!isValidEmail(email.getText().toString().trim())){
                                email.setError("Enter a valid address");
                            }
                        }

                        public final boolean isValidEmail(String target) {
                            if (target == null) {
                                return false;
                            } else {
                                //android Regex to check the email address Validation
                                return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
                            }
                        }
                    });


                    final TextView consejo = new TextView(CargaInicio.this);
                    consejo.setText("For security, please insert password more than 4 words with special characters and numbers");
                    consejo.setGravity(Gravity.CENTER_HORIZONTAL);

                    final TextView espacio = new TextView(CargaInicio.this);
                    espacio.setText("");

                    final TextView medida = new TextView(CargaInicio.this);
                    medida.setText("Password Length");
                    medida.setGravity(Gravity.CENTER_HORIZONTAL);

                    final EditText password = new EditText(CargaInicio.this);
                    password.setGravity(Gravity.CENTER_HORIZONTAL);
                    password.setInputType(InputType.TYPE_CLASS_TEXT
                            | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    password.setHint("Password");

                    password.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {

                            //CALCULATE LENGTH TO STRING
                            calcularMagnitud(s.toString());
                        }

                        private void calcularMagnitud(String s) {

                            //CHARACTER TYPES
                            int Mayusculas = 0, Minusculas = 0, Numeros = 0, Especiales = 0, Otros = 0, MagnitudTexto = 0;
                            char c;
                            int passwordLength = s.length();

                            if (passwordLength == 0) {
                                medida.setText("Please write a password");
                                medida.setTextColor(Color.RED);
                                return;
                            }
                            if (passwordLength <= 5) {
                                MagnitudTexto = 1;
                            } else if (passwordLength <= 10) {
                                MagnitudTexto = 2;
                            } else {
                                MagnitudTexto = 3;
                            }

                            // VERIFY CHARACTER WHEN IS WRITED
                            for (int i = 0; i < passwordLength; i++) {
                                c = s.charAt(i);

                                //VALIDATIONS OF ALL CHARACTER TYPES
                                if (c >= 'a' && c <= 'z') {
                                    if (Minusculas == 0)
                                        MagnitudTexto++;
                                    Minusculas = 1;
                                } else if (c >= 'A' && c <= 'Z') {
                                    if (Mayusculas == 0) MagnitudTexto++;
                                    Mayusculas = 1;
                                } else if (c >= '0' && c <= '9') {
                                    if (Numeros == 0) MagnitudTexto++;
                                    Numeros = 1;
                                } else if (c == '_' || c == '@') {
                                    if (Especiales == 0) MagnitudTexto += 1;
                                    Especiales = 1;
                                } else {
                                    if (Otros == 0) MagnitudTexto += 2;
                                    Otros = 1;
                                }
                            }

                            if (MagnitudTexto <= 3) {
                                medida.setText("Password Strength : LOW");
                                medida.setTextColor(Color.RED);
                            } else if (MagnitudTexto <= 6) {
                                medida.setText("Password Strength : MEDIUM");
                                medida.setTextColor(Color.YELLOW);
                            } else if (MagnitudTexto <= 9) {
                                medida.setText("Password Strength : HIGH");
                                medida.setTextColor(Color.GREEN);
                            }
                        }
                    });

                    LinearLayout acomodar = new LinearLayout(CargaInicio.this);
                    acomodar.setOrientation(LinearLayout.VERTICAL);
                    acomodar.addView(email);
                    acomodar.addView(medidacorreo);
                    acomodar.addView(password);
                    acomodar.addView(consejo);
                    acomodar.addView(espacio);
                    acomodar.addView(medida);
                    registro.setView(acomodar);

                    registro.setCancelable(false);

                    registro.setOnShowListener(new DialogInterface.OnShowListener() {

                        @Override
                        public void onShow(DialogInterface dialogInterface) {

                            Button positivo = (registro).getButton(AlertDialog.BUTTON_POSITIVE);
                            positivo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                    SqliteDb sdb = new SqliteDb(getApplicationContext());
                                    SQLiteDatabase insertar = sdb.getWritableDatabase();

                                    ContentValues datos = new ContentValues();
                                    datos.put("Correo_Usuario", email.getText().toString());
                                    datos.put("Contrasena", password.getText().toString());
                                    insertar.insert("LISTAUSUARIOS", null, datos);

                                    insertar.close();
                                    registro.dismiss();

                                    Toast.makeText(getApplicationContext(), "Usuario registrado", Toast.LENGTH_SHORT).show();

                                    Intent redireccion = new Intent(CargaInicio.this, Inicio.class);
                                    startActivity(redireccion);
                                    finish();
                                }
                            });

                            Button neutral = (registro).getButton(AlertDialog.BUTTON_NEUTRAL);
                            neutral.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View view) {
                                    email.setText("");
                                    password.setText("");
                                }
                            });

                            Button negativo = (registro).getButton(AlertDialog.BUTTON_NEGATIVE);
                            negativo.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    registro.dismiss();
                                    finish();
                                }
                            });
                        }
                    });
                    registro.show();
                }
            }
        }, duracion_splash);


    }
}