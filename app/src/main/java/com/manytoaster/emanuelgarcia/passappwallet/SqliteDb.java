package com.manytoaster.emanuelgarcia.passappwallet;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SqliteDb extends SQLiteOpenHelper {

    SqliteDb(Context context){
       super(context, "passappwalletbase", null, 4);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE LISTAUSUARIOS(ClaveUsuario INTEGER PRIMARY KEY AUTOINCREMENT, Correo_Usuario TEXT, Contrasena TEXT)");

        db.execSQL("CREATE TABLE LISTACONTRASENAS(idPass INTEGER PRIMARY KEY AUTOINCREMENT, Contrasenas_lista TEXT, Numero INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS LISTAUSUARIOS");
        db.execSQL("DROP TABLE IF EXISTS LISTACONTRASENAS");

        onCreate(db);
    }
}
