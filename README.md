# PassAppWallet
---
Es una aplicacion Android en la que tiene como objetivo hacer una funcion de una carpeta de contraseñas usando encriptacion y cifrado para mantenerlas seguras y usarlas en cualquier momento.

---
## Version 1.0 D: 19/12/18
- Cuenta con SQLite para almacenar los datos
- Solo puede encriptar las contraseñas
- Usan encriptacion AES 256 

![Imgur](https://i.imgur.com/QFM9hs0.png)       ![Imgur](https://i.imgur.com/RNRFAbJ.png)       ![Imgur](https://i.imgur.com/iYBz2DQ.png)       ![Imgur](https://i.imgur.com/nM5npDb.png)       ![Imgur](https://i.imgur.com/gIfOayZ.png)       ![Imgur](https://i.imgur.com/f5elmSZ.png)


---
## Version 1.1 D: 04/01/19
- Cuenta con SQLite para almacenar los datos
- Usa encriptacion AES 256
- Encripta y desencripta datos
- Los datos del usuario pueden verse en la pestaña lateral
- Cuenta con una pantalla de bloqueo en la que solo puede acceder con la contraseña original 

![Imgur](https://i.imgur.com/lJcnLXU.png)      ![Imgur](https://i.imgur.com/sQ74ul6.png)    ![Imgur](https://i.imgur.com/FMn57am.png)
![Imgur](https://i.imgur.com/FciG1Ou.png)       ![Imgur](https://i.imgur.com/1yi1i1R.png)